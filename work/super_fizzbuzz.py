class SetCondition():
    def set(self):
        conditions = [FizzFizzBuzzBuzz(), BuzzBuzz(), FizzFizz(), FizzBuzz(), Fizz(), Buzz(), NoFizzBuzz()]
        return SuperFizzbuzz(conditions)


class SuperFizzbuzz():
    def __init__(self, conditions):
        self.conditions = conditions

    def check_condition(self, num):
        for condition in self.conditions:
            if condition.check_condition(num):
                return condition.check_condition(num)


class Fizz():
    def check_condition(self, num):
        if num % 3 == 0:
            return 'Fizz'


class Buzz():
    def check_condition(self, num):
        if num % 5 == 0:
            return 'Buzz'


class FizzBuzz():
    def check_condition(self, num):
        if num % 3 == 0 and num % 5 == 0:
            return 'FizzBuzz'


class FizzFizz():
    def check_condition(self, num):
        if num % 9 == 0:
            return 'FizzFizz'


class BuzzBuzz():
    def check_condition(self, num):
        if num % 25 == 0:
            return 'BuzzBuzz'


class FizzFizzBuzzBuzz():
    def check_condition(self, num):
        if num % 9 == 0 and num % 25 == 0:
            return 'FizzFizzBuzzBuzz'


class NoFizzBuzz():
    def check_condition(self, num):
        return 'NoFizzBuzz'


if __name__ == '__main__':
    result = SetCondition().set()
    for i in range(1, 226):
        print(f'{i} got {result.check_condition (i) }')
