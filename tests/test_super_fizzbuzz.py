import unittest
import work.super_fizzbuzz as super_fizzbuzz

class TestFizzBuzz(unittest.TestCase):

    def test_give_1_shoud_be_no_fizzbuzz(self):
        result = super_fizzbuzz.SetCondition().set()
        self.assertEqual(result.check_condition(1), 'NoFizzBuzz', 'Should be NoFizzBuzz')

    def test_give_3_shoud_be_fizz(self):
        result = super_fizzbuzz.SetCondition().set()
        self.assertEqual(result.check_condition(3), 'Fizz', 'Should be Fizz')

    def test_give_5_shoud_be_buzz(self):
        result = super_fizzbuzz.SetCondition().set()
        self.assertEqual(result.check_condition(5), 'Buzz', 'Should be Buzz')

    def test_give_9_shoud_be_fizzfizz(self):
        result = super_fizzbuzz.SetCondition().set()
        self.assertEqual(result.check_condition(9), 'FizzFizz', 'Should be FizzFizz')

    def test_give_15_shoud_be_fizzbuzz(self):
        result = super_fizzbuzz.SetCondition().set()
        self.assertEqual(result.check_condition(15), 'FizzBuzz', 'Should be FizzBuzz')

    def test_give_30_shoud_be_fizzbuzz(self):
        result = super_fizzbuzz.SetCondition().set()
        self.assertEqual(result.check_condition(30), 'FizzBuzz', 'Should be FizzBuzz')

    def test_give_25_shoud_be_buzzbuzz(self):
        result = super_fizzbuzz.SetCondition().set()
        self.assertEqual(result.check_condition(25), 'BuzzBuzz', 'Should be BuzzBuzz')

    def test_give_225_shoud_be_fizzfizzbuzzbuzz(self):
        result = super_fizzbuzz.SetCondition().set()
        self.assertEqual(result.check_condition(225), 'FizzFizzBuzzBuzz', 'Should be FizzFizzBuzzBuzz')

    def test_give_675_shoud_be_fizzfizzbuzzbuzz(self):
        result = super_fizzbuzz.SetCondition().set()
        self.assertEqual(result.check_condition(675), 'FizzFizzBuzzBuzz', 'Should be FizzFizzBuzzBuzz')

    def test_give_1125_shoud_be_fizzfizzbuzzbuzz(self):
        result = super_fizzbuzz.SetCondition().set()
        self.assertEqual(result.check_condition(1125), 'FizzFizzBuzzBuzz', 'Should be FizzFizzBuzzBuzz')


if __name__ == '__main__':
    unittest.main()
